export interface IException {
    code: string, // Код исключения
    description: string, // Описание исключения
    type: string, // Тип исключения
}

export const exceptions: Map<string, IException> = new Map([
    ['C001', {code: 'C001', description: 'Ошибка получения каталога товаров', type: 'ERROR'}],
    ['C002', {code: 'C002', description: 'Каталог пуст', type: 'ERROR'}],
    ['C003', {code: 'C003', description: 'Ошибка получения товара', type: 'ERROR'}],
    ['C004', {code: 'C004', description: 'Товар не найден', type: 'ERROR'}],
    ['A001', {code: 'A001', description: 'Ошибка получения списка прикреплений', type: 'ERROR'}],
    ['A002', {code: 'A002', description: 'Прикреплений нет', type: 'ERROR'}],
    ['M001', {code: 'M001', description: 'Ошибка при отправке заказа', type: 'ERROR'}],
    ['B007', {code: 'B007', description: 'Ошибка при создании покупателя', type: 'ERROR'}],
    ['S001', {code: 'S001', description: 'Не достаточно данных', type: 'ERROR'}],
    ['S002', {code: 'S002', description: 'Ошибка сохранения', type: 'ERROR'}],
    ['S003', {code: 'S003', description: 'Ошибка поиска', type: 'ERROR'}],
]);