import {IException} from "./exceptions.model";

export class ResponseModel<IDataType> {

    public readonly data: IDataType;
    public readonly error: IException;

    public constructor(data: IDataType = null, error: IException = null) {
        this.data = data;
        this.error = error;
    }
}