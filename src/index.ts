﻿import {routerCatalog} from "./controllers/catalog.controller";

require('rootpath')();
import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import {config} from './config';
import {routerAttachments} from "./controllers/attachments.controller";
import {routerMail} from "./controllers/mail.controller";
import {socialRouter} from "./controllers/social.controller";
import {statisticInfoRouter} from "./controllers/statisticInfoRouter.controller";

const app = express();
app.use(express.static('src/public'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// routes
app.use('/catalog-request', routerCatalog);
app.use('/attachments-request', routerAttachments);
app.use('/mail-request', routerMail);
app.use('/social-request', socialRouter);
app.use('/statisticInfo-request', statisticInfoRouter);
app.use((req, res) => {
    // Use res.sendfile, as it streams instead of reading the file into memory.
    res.sendfile(__dirname + '/public/index.html');
});


// start server
const server = app.listen(config.port, () => {
    console.log('Server listening on port ' + config.port);
});