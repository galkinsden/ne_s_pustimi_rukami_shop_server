﻿export const config = {
    connectionString: 'mongodb://localhost:27017/test',
    apiUrl: 'http://localhost:4001',
    mail: {
        user: 'nespustimirukami@gmail.com',
        from: 'nespustimirukami@gmail.com',
        to: 'nespustimirukami@gmail.com',
        password: 'Dtr2ex9aWX',
        subject: 'Заказ №'
    },
    telegram: {
        ne_s_pustimi_rukami_test: {
            access_token: '383534863:AAFOUpAoEBXazXF_NZuQpD8u9iktY6UwKw0',
        },
        ne_s_pustimi_rukami: {
            id: '494724359'
        }
    },
    port: 80
};

export enum EMongodbCollections {
    CATALOG = 'catalog',
    ATTACHMENTS = 'attachments',
    BUYERS = 'buyers',
    SOCIAL = 'social'
}