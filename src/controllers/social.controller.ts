import * as Router from 'express';
import {socialService} from "../services/social.service";

export const routerCatalog = Router();
import {exceptions} from '../models/exceptions.model';
import {statisticInfoRouterClass} from "./statisticInfoRouter.controller";

class SocialRouter {

    public getAll(req, res): void {
        socialService.getAll().then(result => {
            res.send(result);
        }).catch(e => {
            res.send(e);
        })
    }

    public getView(req, res): void {

        if (!req.params.id) {
            res.status(400).send(exceptions.get('S001'));
            return;
        }

        socialService.getView(req.params.id).then(result => {
            res.send(result);
        }).catch(e => {
            res.send(e);
        })
    }

    public getPurchases(req, res): void {
        if (!req.params.id) {
            res.status(400).send(exceptions.get('S001'));
            return;
        }
        socialService.getPurchases(req.params.id).then(result => {
            res.send(result);
        }).catch(e => {
            res.send(e);
        })
    }

    public getLike(req, res): void {
        if (!req.params.id) {
            res.status(400).send(exceptions.get('S001'));
            return;
        }
        socialService.getLike(req.params.id).then(result => {
            res.send(result);
        }).catch(e => {
            res.send(e);
        })
    }

    public getComment(req, res): void {

        if (!req.params.id) {
            res.status(400).send(exceptions.get('S001'));
            return;
        }

        socialService.getComments(req.params.id).then(result => {
            res.send(result);
        }).catch(e => {
            res.send(e);
        })
    }

    public setPurchases(req, res): void {
        if (!req.body.id) {
            res.status(400).send(exceptions.get('S001'));
            return;
        }
        statisticInfoRouterClass.incPurchasesCount();
        socialService.setPurchases(req.body.id, req.body.count).then(result => {
            res.send(result);
        }).catch(e => {
            res.send(e);
        })
    }

    public setView(req, res): void {
        if (!req.body.id) {
            res.status(400).send(exceptions.get('S001'));
            return;
        }

        socialService.setView(req.body.id).then(result => {
            res.send(result);
        }).catch(e => {
            res.send(e);
        })
    }

    public setLike(req, res): void {
        if (!req.body.id) {
            res.status(400).send(exceptions.get('S001'));
            return;
        }

        socialService.setLike(req.body.id).then(result => {
            res.send(result);
        }).catch(e => {
            res.send(e);
        })
    }

    public setComment(req, res): void {

        if (!req.body.id || !req.body.text) {
            res.status(400).send(exceptions.get('S001'));
            return;
        }

        socialService.setComment(req.body.id, req.body.text).then(result => {
            res.send(result);
        }).catch(e => {
            res.send(e);
        })
    }
}

const socialRouterClass = new SocialRouter();
export const socialRouter = Router();
// routes
socialRouter.get('/', socialRouterClass.getAll);
socialRouter.get('/view/:id', socialRouterClass.getView);
socialRouter.get('/purchases/:id', socialRouterClass.getPurchases);
socialRouter.get('/likes/:id', socialRouterClass.getLike);
socialRouter.get('/comment/:id', socialRouterClass.getComment);
socialRouter.post('/view', socialRouterClass.setView);
socialRouter.post('/purchases', socialRouterClass.setPurchases);
socialRouter.post('/likes', socialRouterClass.setLike);
socialRouter.post('/comment', socialRouterClass.setComment);