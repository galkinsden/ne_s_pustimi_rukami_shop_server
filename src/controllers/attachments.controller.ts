import * as Router from 'express';
import {ResponseModel} from "../models/response.model";
import {attachmentsService} from "../services/attachments.service";

const getById = (req, res) => {
    attachmentsService.getById(req.body.id)
        .then((responseModel) => {
            res.send(responseModel);
        })
        .catch((responseModel) => {
            res.status(400).send(responseModel);
        });
};

export const routerAttachments = Router();

// routes
routerAttachments.post('/', getById);