import * as Router from 'express';
import {ResponseModel} from "../models/response.model";
import * as moment from 'moment';

export class StatisticInfoRouter {
    private peopleCount: number = Math.round(Math.random() * 100);
    private purchasesCount: number = Math.round(Math.random() * 100);
    private lastChange: number = moment().valueOf();

    constructor() {
    }

    getStatistic(req, res): void {
        this.formStatistic();
        res.send(new ResponseModel({
            peopleCount: this.peopleCount,
            purchasesCount: this.purchasesCount
        }))
    }

    incPeopleCount(): void {
        this.peopleCount++;
    }

    incPurchasesCount(): void {
        this.purchasesCount++;
    }

    private  getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    private formStatistic(): void {
        if(moment().isAfter(this.lastChange, 'day')) {
            this.purchasesCount = this.getRandomInt(20, 150) + 1;
        }
        this.peopleCount = Math.round(Math.random() * 100) + 1;
        this.lastChange = moment().valueOf();
    }
}

export const statisticInfoRouterClass = new StatisticInfoRouter();
export const statisticInfoRouter = Router();

statisticInfoRouter.get('/', statisticInfoRouterClass.getStatistic.bind(statisticInfoRouterClass));