import * as Router from 'express';
import {mailService} from "../services/mail.service";

const sendMail = (req, res) => {
    mailService.sendMail(req.body.data)
        .then((responseModel) => {
            res.send(responseModel);
        })
        .catch((responseModel) => {
            res.status(400).send(responseModel);
        });
};

export const routerMail = Router();

// routes
routerMail.post('/', sendMail);