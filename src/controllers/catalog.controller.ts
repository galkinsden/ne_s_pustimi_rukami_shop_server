import * as Router from 'express';
import {ResponseModel} from "../models/response.model";
import {catalogService} from "../services/catalog.service";
import {statisticInfoRouterClass} from "./statisticInfoRouter.controller";

const getAll = (req, res) => {
    statisticInfoRouterClass.incPeopleCount();
    catalogService.getAll(req.body.id)
        .then((responseModel) => {
            res.send(responseModel);
        })
        .catch((responseModel) => {
            res.status(400).send(responseModel);
        });
};

const getById = (req, res) => {
    catalogService.getById(req.body.id)
        .then((responseModel) => {
            res.send(responseModel);
        })
        .catch((responseModel) => {
            res.status(400).send(responseModel);
        });
};

const getByIds = (req, res) => {
    catalogService.getByIds(req.body.ids)
        .then((responseModel) => {
            res.send(responseModel);
        })
        .catch((responseModel) => {
            res.status(400).send(responseModel);
        });
};

export const routerCatalog = Router();

// routes
routerCatalog.get('/', getAll);
routerCatalog.post('/id', getById);
routerCatalog.post('/ids', getByIds);