import * as _ from 'lodash';

const uuidv1 = require('uuid/v1');
import * as Q from 'q';
import {ResponseModel} from '../models/response.model';
import {exceptions} from '../models/exceptions.model';
import {config, EMongodbCollections} from '../config';
import * as moment from 'moment';
import {catalogService} from "./catalog.service";

const nodemailer = require('nodemailer');
const xoauth2 = require('xoauth2');
import * as mongo from 'mongoskin';

const db = mongo.db(config.connectionString, {native_parser: true});
db.bind(EMongodbCollections.BUYERS);

import * as TelegramBot from 'node-telegram-bot-api';
import {IBuyerFull} from "../interface/buyer-full.interface";
import {EStatus} from "../enum/status.enum";
import {EPlaceType} from "../enum/placetype.enum";

export const bot: any = new TelegramBot(config.telegram.ne_s_pustimi_rukami_test.access_token);

export enum ESaleTypes {
    like = 'like'
}

export interface IGift {
    id: string;
    count: number;
    saleTypes?: ESaleTypes[]
}

const insertBuyer = (buyer: IBuyerFull) => {

    const deferred = Q.defer();

    db[EMongodbCollections.BUYERS].insertOne(buyer, (err, buyer) => {

        if (err) deferred.reject(new ResponseModel(false, exceptions.get('B007')));

        if (buyer) {
            deferred.resolve(new ResponseModel(true));
        } else {
            deferred.resolve(new ResponseModel(false, exceptions.get('B007')));
        }

    });

    return deferred.promise;
};

export interface IMailSendRequest {
    address: string;
    date: string;
    comment: string;
    name: string;
    phone: string;
    gifts: IGift[];
    placeType: EPlaceType;
}

const getRandomInt = (min: number, max: number): number => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

const sendMail = (data: IMailSendRequest) => {
    const deferred = Q.defer();

    if (data.gifts && data.gifts.length) {
        const dataMail = {
            name: data && data.name || '',
            gifts: '',
            address: data && data.address || '',
            phone: data && data.phone || '',
            dateDelivery: data && data.date && moment(data.date).format('DD.MM.YYYY') || '',
            dateOrder: moment().add(3, 'hours').format('DD.MM.YYYY HH:mm:ss'),
            comment: data && data.comment || '',
            placeType: data && data.placeType ? (data.placeType === 'job' ? 'Офисное здание' : data.placeType === 'home' ? 'Домашний адрес' : '') : ''
        };

        catalogService.getByIds(data.gifts.map((g) => g.id))
            .then((giftsResponse) => {
                if (giftsResponse && giftsResponse.data && giftsResponse.data.length) {
                    dataMail.gifts = data.gifts.reduce((acc, el: IGift) => {
                        const elem = giftsResponse.data.find((g: any) => g.id === el.id);
                        return acc + `${elem.name} : ${el.count}шт, `
                    }, '')
                } else {
                    dataMail.gifts = data.gifts && data.gifts.length ? data.gifts.reduce((acc, el: IGift) => {
                        return acc + `${el.id} : ${el.count}шт, `
                    }, '') : '';
                }
            })
            .catch(() => {
                dataMail.gifts = data.gifts && data.gifts.length ? data.gifts.reduce((acc, el: IGift) => {
                    return acc + `${el.id} : ${el.count}шт, `
                }, '') : '';
            })
            .finally(() => {
                const htmlTelegram = `<b>${config.mail.subject}</b> <code>${getRandomInt(10000, 99999)}</code>
                        Имя: <code>${dataMail.name}</code>
                        Название заказа и количество: <code>${dataMail.gifts}</code>
                        Адрес: <code>${dataMail.address}</code>
                        Офисное здание/Домашний адрес: <code>${dataMail.placeType}</code>
                        Телефон: <code>${dataMail.phone}</code>
                        Желаемая дата получения заказа: <code>${dataMail.dateDelivery}</code>
                        Дата и время совершения заказа: <code>${dataMail.dateOrder}</code>
                        Комментарий: <code>${dataMail.comment}</code>`;

                bot.sendMessage(config.telegram.ne_s_pustimi_rukami.id, htmlTelegram, {
                    parse_mode: 'HTML'
                })
                    .then(() => {
                        deferred.resolve(new ResponseModel(true));
                    })
                    .catch(() => {
                        deferred.resolve(new ResponseModel(false, exceptions.get('M001')));
                    });

                const dataSave: IBuyerFull = {
                    id: uuidv1(),
                    name: data && data.name || '',
                    orders: data.gifts,
                    address: data && data.address || '',
                    phone: data && data.phone || '',
                    dateDelivery: data && data.date && moment(data.date).toISOString() || '',
                    dateOrder: moment().add(3, 'hours').toISOString(),
                    comment: data && data.comment || '',
                    placeType: data && data.placeType || null,
                    status: EStatus.NEWEST
                };

                insertBuyer(dataSave);
            })
    }


    deferred.resolve(new ResponseModel(true));
    return deferred.promise;
};

export const mailService = {
    sendMail: sendMail.bind(this)
};