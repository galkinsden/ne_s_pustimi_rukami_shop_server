import {config, EMongodbCollections} from "../config";
import * as Q from 'q';
import * as mongo from 'mongoskin';
import {exceptions} from '../models/exceptions.model';
import {EModes, ISocialFullInfo} from "../interface/social.interface";
import {ResponseModel} from "../models/response.model";

const db = mongo.db(config.connectionString, {native_parser: true});
db.bind(EMongodbCollections.SOCIAL);


class SocialService {

    private isHit(element: ISocialFullInfo, fullInfo: any): boolean {
        let currentLikes: number = (element && element.like || 0);
        let otherLikes: number = (fullInfo && fullInfo.likes || 0) - currentLikes;
        let currentViews: number = (element && element.view || 0);
        let otherViews: number = (fullInfo && fullInfo.view || 0) - currentViews;
        let currentPurchases: number = (element && element.purchases || 0);
        let otherPurchases: number = (fullInfo && fullInfo.purchases || 0) - currentViews;
        let currentComments: number = (element && Array.isArray(element.comments) && element.comments.length || 0);
        let otherComments: number = (fullInfo && Array.isArray(fullInfo.comments) && fullInfo.comments.length || 0) - currentComments;
        return (otherLikes * 0.4 + otherViews * 0.1 +
            otherPurchases + otherComments * 0.7) * 0.15 <
            (currentLikes * 0.4 + currentViews * 0.1 + currentPurchases +
                currentComments * 0.7);
    }

    private countAllSocialInfo(res: ISocialFullInfo[]): Object {
        return res ? res.reduce((result, element: ISocialFullInfo) => {
            result.likes += element.like || 0;
            result.views += element.view || 0;
            result.purchases += element.purchases || 0;
            result.comments += Array.isArray(element.comments) ? element.comments.length : 0;
            return result;
        }, {
            likes: 0, // 0.4
            views: 0, // 0.1
            purchases: 0, // 1
            comments: 0, // 0.7
        }) : {
            likes: 0, // 0.4
            views: 0, // 0.1
            purchases: 0, // 1
            comments: 0, // 0.7
        };
    }

    getAll(): Promise<ISocialFullInfo[]> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].find({}).toArray((err, res) => {
            if (err) {
                deferred.reject(new ResponseModel(null, exceptions.get('S003')));
            }


            let tmp = this.countAllSocialInfo(res);

            res = res.map((element: ISocialFullInfo, i: number, arr: ISocialFullInfo[]) => {
                if(!element.like) {
                    this.addLike(element.id, this.getRandomInt(30, 100));
                } else if(element.like < 10) {
                   this.setLike(element.id, this.getRandomInt(30, 50))
                }

                if(!element.like) {
                    this.addView(element.id, this.getRandomInt(300, 500));
                } else if(element.like < 10) {
                    this.setView(element.id, this.getRandomInt(30, 50));
                }

                if(!element.purchases) {
                    this.addPurchases(element.id, this.getRandomInt(20, 40));
                } else if(element.like < 10) {
                    this.setPurchases(element.id, this.getRandomInt(5, 10));
                }

                if (this.isHit(element, tmp)) {
                    if (Array.isArray(element.modes)) {
                        element.modes.push(EModes.hit)
                    } else {
                        element.modes = [EModes.hit]
                    }
                }

                return element;
            });
            deferred.resolve(new ResponseModel(res, exceptions.get('S003')));
        });

        return deferred.promise;
    }

    getView(id: string): Promise<ISocialFullInfo[]> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].find({id: id}, {view: 1}).toArray((err, res) => {
            if (err) {
                deferred.reject(new ResponseModel(null, exceptions.get('S003')));
            }

            deferred.resolve(new ResponseModel(res, null));
        });

        return deferred.promise;
    }

    getLike(id: string): Promise<ISocialFullInfo[]> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].find({id: id}, {like: 1}).toArray((err, res) => {
            if (err) {
                deferred.reject(new ResponseModel(null, exceptions.get('S003')));
            }

            deferred.resolve(new ResponseModel(res, null));
        });

        return deferred.promise;
    }

    getComments(id: string): Promise<ISocialFullInfo[]> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].find({id: id}, {comments: 1}).toArray((err, res) => {
            if (err) {
                deferred.reject(new ResponseModel(null, exceptions.get('S003')));
            }

            deferred.resolve(new ResponseModel(res, null));
        });

        return deferred.promise;
    }

    getPurchases(id: string): Promise<ISocialFullInfo> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].find({id: id}, {purchases: 1}).toArray((err, res) => {
            if (err) {
                deferred.reject(new ResponseModel(null, exceptions.get('S003')));
            }

            deferred.resolve(new ResponseModel(res, null));
        });

        return deferred.promise;
    }

    setView(id: string, count: number = 1): Promise<boolean> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].update(
            {"id": id},
            {"$inc": {"view": count}},
            {upsert: true},
            (err, res) => {
                if (err) {
                    deferred.resolve(new ResponseModel(false, null));
                }

                deferred.resolve(new ResponseModel(true, null));
            }
        );
        return deferred.promise;
    }

    setLike(id: string, count: number = 1): Promise<boolean> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].update(
            {"id": id},
            {"$inc": {"like": count}},
            {upsert: true},
            (err, res) => {
                if (err) {
                    deferred.resolve(new ResponseModel(false, null));
                }

                deferred.resolve(new ResponseModel(true, null));
            }
        )
        return deferred.promise;
    }

    setComment(id: string, text: string): Promise<boolean> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].update(
            {"id": id},
            {"$set": {"comments.$": text}},
            {upsert: true},
            (err, res) => {
                if (err) {
                    deferred.resolve(new ResponseModel(false, null));
                }

                deferred.resolve(new ResponseModel(true, null));
            }
        )
        return deferred.promise;
    }

    setPurchases(id: string, count: number): Promise<boolean> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].update(
            {"id": id},
            {"$inc": {"purchases": count}},
            {upsert: true},
            (err, res) => {
                if (err) {
                    deferred.resolve(new ResponseModel(false, null));
                }

                deferred.resolve(new ResponseModel(true, null));
            }
        )
        return deferred.promise;
    }

    addPurchases(id: string, count: number): Promise<boolean> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].update(
            {"id": id},
            {"$set": {"purchases": count}},
            {upsert: true},
            (err, res) => {
                if (err) {
                    deferred.resolve(new ResponseModel(false, null));
                }

                deferred.resolve(new ResponseModel(true, null));
            }
        )
        return deferred.promise;
    }

    addView(id: string, count: number = 1): Promise<boolean> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].update(
            {"id": id},
            {"$set": {"view": count}},
            {upsert: true},
            (err, res) => {
                if (err) {
                    deferred.resolve(new ResponseModel(false, null));
                }

                deferred.resolve(new ResponseModel(true, null));
            }
        );
        return deferred.promise;
    }

    addLike(id: string, count: number = 1): Promise<boolean> {
        let deferred = Q.defer();
        db[EMongodbCollections.SOCIAL].update(
            {"id": id},
            {"$set": {"like": count}},
            {upsert: true},
            (err, res) => {
                if (err) {
                    deferred.resolve(new ResponseModel(false, null));
                }

                deferred.resolve(new ResponseModel(true, null));
            }
        )
        return deferred.promise;
    }

    private getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}

export const socialService = new SocialService();