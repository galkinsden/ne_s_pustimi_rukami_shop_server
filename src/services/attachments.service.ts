import * as _ from 'lodash';
import * as Q from 'q';
import * as mongo from 'mongoskin';
import {config, EMongodbCollections} from '../config';
import {ResponseModel} from '../models/response.model';
import {exceptions} from '../models/exceptions.model';
import * as uuidv1 from 'uuid/v1';

const db = mongo.db(config.connectionString, { native_parser: true });
db.bind(EMongodbCollections.ATTACHMENTS);

const getById = (id: string) => {
    const deferred = Q.defer();
    db[EMongodbCollections.ATTACHMENTS].findOne({id: id}, {'_id': 0}, (err, attachments) => {
        if (err) deferred.reject(new ResponseModel(null, exceptions.get('A001')));

        if (attachments) {
            deferred.resolve(new ResponseModel(attachments));
        } else {
            deferred.resolve(new ResponseModel(null, exceptions.get('A002')));
        }
    });

    return deferred.promise;
};

export const attachmentsService = {
    getById: getById.bind(this)
};