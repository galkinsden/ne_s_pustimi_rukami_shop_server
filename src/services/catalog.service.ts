import * as _ from 'lodash';
import * as Q from 'q';
import * as mongo from 'mongoskin';
import {config, EMongodbCollections} from '../config';
import {ResponseModel} from '../models/response.model';
import {exceptions} from '../models/exceptions.model';
import * as uuidv1 from 'uuid/v1';

const db = mongo.db(config.connectionString, { native_parser: true });
db.bind(EMongodbCollections.CATALOG);

const getAll = () => {
    const deferred = Q.defer();

    db[EMongodbCollections.CATALOG].find({}).toArray((err, catalog) => {
        if (err) deferred.reject(new ResponseModel(null, exceptions.get('C001')));

        if (catalog && catalog.length) {
            deferred.resolve(new ResponseModel({list: catalog}));
        } else {
            deferred.resolve(new ResponseModel(null, exceptions.get('C002')));
        }

    });

    return deferred.promise;
};

const getById = (id: string) => {
    const deferred = Q.defer();

    db[EMongodbCollections.CATALOG].findOne({id: id}, (err, gift) => {
        if (err) deferred.reject(new ResponseModel(null, exceptions.get('C003')));

        if (gift) {
            deferred.resolve(new ResponseModel(gift));
        } else {
            deferred.resolve(new ResponseModel(null, exceptions.get('C004')));
        }

    });

    return deferred.promise;
};

const getByIds = (ids: string[]) => {
    const deferred = Q.defer();
    db[EMongodbCollections.CATALOG].find({$or: ids.map((id: string) => ({id: id}))}).toArray((err, catalog) => {
        if (err) deferred.reject(new ResponseModel([], exceptions.get('C001')));

        if (catalog && catalog.length) {
            deferred.resolve(new ResponseModel(catalog));
        } else {
            deferred.resolve(new ResponseModel([], exceptions.get('C002')));
        }

    });

    return deferred.promise;
};

export const catalogService = {
    getAll: getAll.bind(this),
    getById: getById.bind(this),
    getByIds: getByIds.bind(this)
};