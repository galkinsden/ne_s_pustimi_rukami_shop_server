export enum EStatus {
    NEWEST = 'NEWEST',
    WAIT = 'WAIT',
    FINISHED = 'FINISHED',
    CANCELED = 'CANCELED',
    OUTSIDE = 'OUTSIDE'
}