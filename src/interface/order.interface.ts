export interface IOrder {
    name: string;
    count: number;
}
