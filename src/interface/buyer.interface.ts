
import {EStatus} from "../enum/status.enum";

export interface IBuyer {
    id: string;
    name: string;
    date: string;
    status: EStatus;
}
