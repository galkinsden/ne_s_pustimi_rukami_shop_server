export enum EModes {
   hit = 'hit'
}
export interface ISocialFullInfo {
    id: string;
    view?: number;
    like?: number;
    purchases?: number;
    comments?: string[];
    modes?: [EModes]
}