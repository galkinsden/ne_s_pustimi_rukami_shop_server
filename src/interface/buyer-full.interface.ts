import {IOrder} from "./order.interface";
import {EPlaceType} from "../enum/placetype.enum";
import {EStatus} from "../enum/status.enum";
import {IGift} from "../services/mail.service";

export interface IBuyerFull {
    id: string;
    name: string;
    comment: string;
    phone: string;
    orders: IGift[];
    address: string;
    dateOrder: string;
    dateDelivery: string;
    status: EStatus;
    placeType: EPlaceType;
}
