var MongoClient = require('mongodb').MongoClient;
MongoClient.connect("mongodb://localhost:27017/test", function (error, db){

    if(error) throw error;

    var docs = [
        require('./attachments/1.json'),
        require('./attachments/2.json'),
        require('./attachments/3.json'),
        require('./attachments/4.json'),
        require('./attachments/5.json'),
        require('./attachments/6.json'),
        require('./attachments/7.json'),
        require('./attachments/8.json'),
        require('./attachments/9.json'),
        require('./attachments/10.json'),
        require('./attachments/11.json'),
        require('./attachments/12.json'),
        require('./attachments/13.json'),
        require('./attachments/14.json'),
        require('./attachments/15.json'),
        require('./attachments/16.json'),
        require('./attachments/17.json'),
        require('./attachments/18.json'),
        require('./attachments/19.json'),
        require('./attachments/20.json'),
        require('./attachments/21.json'),
        require('./attachments/22.json'),
        require('./attachments/23.json'),
    ];

    db.collection("attachments").remove({}, function(error) {
        if (error) {
            console.log('Не удалось дропнуть коллекцию =(');
            db.close();
        } else {
            db.collection('attachments').insertMany(docs, function(error, inserted) {
                if(error) {
                    console.error('Не удалось добавить составляющие в коллекию =(');
                } else {
                    console.log('Составляющие обновлены =)');
                }
                db.close();
            });
        }
    });

});
