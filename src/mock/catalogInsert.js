var MongoClient = require('mongodb').MongoClient;
MongoClient.connect("mongodb://localhost:27017/test", function (error, db){

    if(error) throw error;

    var docs = [
        require('./catalog/1.json'),
        require('./catalog/2.json'),
        require('./catalog/3.json'),
        require('./catalog/4.json'),
        require('./catalog/5.json'),
        require('./catalog/6.json'),
        require('./catalog/7.json'),
        require('./catalog/8.json'),
        require('./catalog/9.json'),
        require('./catalog/10.json'),
        require('./catalog/11.json'),
        require('./catalog/12.json'),
        require('./catalog/13.json'),
        require('./catalog/14.json'),
        require('./catalog/15.json'),
        require('./catalog/16.json'),
        require('./catalog/17.json'),
        require('./catalog/18.json'),
        require('./catalog/19.json'),
        require('./catalog/20.json'),
        require('./catalog/21.json'),
        require('./catalog/22.json'),
        require('./catalog/23.json')
    ];

    db.collection("catalog").remove({}, function(error) {
        if (error) {
            console.log('Не удалось дропнуть коллекцию =(');
            db.close();
        } else {
            db.collection('catalog').insertMany(docs, function(error, inserted) {
                if(error) {
                    console.error('Не удалось добавить товары в каталог =(');
                } else {
                    console.log('Каталог обновлен =)');
                }
                db.close();
            });
        }
    });

});
